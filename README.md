# kotlin tutoriel

Kotlin est un langage basé sur JAVA et donc des meme Techno 

Ce tutoriel montre plusieurs fonctionnement 
De la base donc des "instruction", création de fonction, POO, 
Mais aussi Des notions un peu plus evolué du concept de la POO 
L'héritage

J'utilise Intellj Community pour faire mon code
J'ai un peu de temps a comprendre comment configurer kotlin 

# Code

## Configuration du projet
> "add configuration" en haut 
>au meme endroit que pour le Run de l'app

>Ensuite ajouter une page Configuration 
>dont vous donnerez le nom que vous voulez

> Et puis dans le premier champs 
>"veuillez selectionner la fonction 
>du main, c'est ce sera le point de 
>départ de l'application

## BASE 
Exercice 1

## POO 
EXERCICE 1 et Exercice 2

## Heritage 
Exercice 2

## Gestion de la bdd 
A venir 

#### Connexion
???

#### MYSQL
????

#### SQLITE
???

### Select 
????

### Crud
????

## Gestion des erreur et test Unitaire

### Test Unitaire
A venir

### ERREUR
    try{
    ...
    }
    
ou

    throw
    
    
* En Kotlin, une exception se gère comme en Java, grâce aux mots-clés try et  
  catch. Contrairement à Java, en Kotlin, une fonction pouvant renvoyer une exception n’a pas besoin du mot-clé  
  throws dans sa signature.
* try , catch et throw sont des expressions en Kotlin et pourront donc renvoyer une 
  valeur. 
* La classe Nothing représente une "valeur inexistante", utilisée notamment 
  par une fonction ne renvoyant qu’une exception (et jamais de valeur).
* L’opérateur Elvis ?: permet de définir une action alternative si l'expression 
  le précédant est égale à nulle.

## Android 
A venir 

# Type de variable 
10              => Int

"Troll"         => String

arrayof(1, 2)   => Array

listOf(1, 2)    => List

false           => Boolean

9.0             => Double...

Le ? aprés les variable permet de nullable une variable

Les diff entre tableau et list => a venir 

is et as => is = instanceof et as permet de caster (l'exception retourner en cas de mauvaise gestion est ClassCastException)

    switch => when(object){
        is Int -> println("c'est un entier")// La fleche sert de case
    ...
    }

