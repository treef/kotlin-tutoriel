class Moto(usine:Usine) : Vehicule(usine) {

    private var nomMoto: String = ""

    init {
        setNomVehiculeInteraction()
        println(println(super.toString()))
    }

    //setter
    protected fun setNomVehiculeInteraction(){
        val interaction = Interaction()
        interaction.envoieMessageIndicationPourString()
        this.nomMoto = interaction.ScanerString()
    }

    override fun toString(): String {
        return "C'est une moto categorie ${this.nomMoto}"
    }
}