class Voiture(usine:Usine) : Vehicule(usine) {

    private var nomVoiture: String = ""

    init {
        println(super.toString())
        this.setNomVehiculeInteraction()

        //Vous pouvez aussi créer une fonction local avec local
    }

    //setter
    protected fun setNomVehiculeInteraction(){
        val interaction = Interaction()
        interaction.envoieMessageIndicationPourString()
        this.nomVoiture = interaction.ScanerString()
    }

    //Vous pouvez également utiliser des extension
    override fun toString(): String {
        return "C'est une voiture categorie ${this.nomVoiture} elle a donc ${this.getNbRoue()} roues, cette voiture a ete construite par ${this.getUsine().getLabel()}"
    }
}