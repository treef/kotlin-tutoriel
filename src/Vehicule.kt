open class Vehicule(private var usine:Usine) {
    private var lab: String = ""
    private var nbRoue:Int = 0
    private var type:String = ""


    //Constructeur primaire
    init {

            this.setUsine(this.usine)
            this.setLabelInteraction()
            this.setNbRoueInteraction()
            this.definiTypeVehicule()


    }

    //setter
    protected fun setLabelInteraction(){
        val interaction = Interaction()
        print("label vehicule : ")
        this.lab = interaction.ScanerString()
    }

    protected fun setNbRoueInteraction(){
        val interaction = Interaction()
        var a = interaction.ScannerTest()
        this.nbRoue = a
    }

    protected fun setUsine(usine:Usine){
        this.usine = usine
    }

    //getter
    fun getLabel():String = this.lab
    fun getNbRoue():Int = this.nbRoue
    fun getType():String = this.type
    fun getUsine():Usine = this.usine

    //Visibilité
    private fun definiTypeVehicule(){
        when (this.nbRoue) {
            1 -> this.type = "Monocycle"
            2 -> this.type = "Moto"
            else -> {
                this.type = "Voiture"
            }
        }
    }

    //overriding exemple
    override fun toString():String {
        return "le vehicule ${this.getLabel()} est un(e) ${getType()}, créé par ${this.getUsine().getLabel()}"
    }
}