class Usine(private var label: String, private var nbProdJour: Int) {

    //setter
    fun setLabel(label: String) {
        this.label = label
    }
    fun setnbProdJour(nb: Int){
        this.nbProdJour = nb
    }

    //getter
    fun getNbProdJour(): Int = this.nbProdJour
    fun getLabel(): String = this.label

    //l'écriture d'une fonction simple est plus rapide
    private fun orthographeVehicule(): String = if (this.getNbProdJour() > 1 ) "vehicules" else "vehicule"

    //On doit overrider toString afin de l'utiliser comme on le veut
    //sinon cela nous rendrait l'emplacement de l'objet
    override fun toString(): String {
        return "Usine '$label' peut construire $nbProdJour ${this.orthographeVehicule()} par jour"
    }
}