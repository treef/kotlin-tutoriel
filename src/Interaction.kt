import java.util.*

class Interaction {

    private val reader = Scanner(System.`in`)

    fun ScanerInt(): Int{
        var a:Int = 0
        try {
            a = reader.nextInt()
        }
        catch (e:InputMismatchException){
            a = reader.nextInt()
        }

        return a
    }

    fun ScannerTest():Int {
        var scan:String = ""
        scan = reader.nextLine()

        var numeric = true

        numeric = scan.matches("-?\\d+(\\.\\d+)?".toRegex())
        //println(numeric)
        //println(scan.matches("-?\\d+(\\.\\d+)?".toRegex()))
        if(!numeric){
            while (!numeric){
                scan = reader.nextLine()
                numeric = scan.matches("-?\\d+(\\.\\d+)?".toRegex())
                //println(numeric)
                //println(scan.matches("-?\\d+(\\.\\d+)?".toRegex()))
            }
        }

        return scan.toInt()
    }

    fun ScanerString(): String = reader.nextLine()
    fun envoieMessageIndicationPourInt() = print("Veuillez Insérer un nombre SVP ")
    fun envoieMessageIndicationPourString() = print("Veuillez Insérer un libelle SVP ")
}