import java.util.*
import extensions.Presentation

fun main(args: Array<String>){

    /*  Exo 1 prise en main des grande lignes du langage
    // Premiere Visualisation
    //Instanciation => pas de new
    var usine1 = Usine("Test1", 27)
    //vous voyer que kotlin est basé sur java, c'est les meme nom d'appel
    //JAVA => System.out.println ; Kotlin => println
    println(usine1.toString())

    usine1.setnbProdJour(29)
    //var fair ref a une valeur modifiable
    var nb:Int = usine1.getNbProdJour()
    println(usine1.toString())

    //val represente une une variable immuable => constante
    val usineRecession = Usine("Test1", 0)
    usineRecession.setnbProdJour(0)
    println(usineRecession.toString())
    */

    /*
    /* Exo 2 */
    //Deuxiéme Visualisation

    //Scanner => permettre d'interargir avec le User
    //Penser a importer la bibliothéque
    val interaction = Interaction()


    print("Entrer un nom ")
    var label: String? = interaction.ScanerString()
    print("Entrer un nombre de prod par jour de l'usine ${label.toUpperCase()} ")
    var nb2: Int? = interaction.ScannerTest()
    var usine2 = Usine(label, nb2)
    println(usine2.toString())


    var vehicule:Voiture = Voiture(usine2)
    print(message = vehicule.toString())
     */

    /* Exercice Extension exemple

    var testExt: String = "maybe"
    println(testExt.Presentation())
     */

    /* Les lambda petit fonction anonyme */

    /*
    data class User(var nom:String, var age:Int)

    val users = listOf(
        User("machin" , 22),
        User("bidule", 12),
        User("truc", 32)
    )

    println( users.maxBy{ it.age } )
    println( users.minBy(User::age) )


    //Le mot-clé  it  à l’intérieur d’une lambda fait toujours référence à l’unique paramètre de celle-ci.
    //Vous l’utiliserez très souvent pour gagner en lisibilité.

    //filter => Itere sur chaque valeur de la list et retourne la valeur si celle respecte la condition
    print(users.filter { it.age >= 20 }
        //itére sur chaque valeur de la list et applique une fonction a
        //chacune des des valeurs
        .map { "${it.nom} est agé de ${it.age}" })

    //Verifie si tous les items verifie la condition
    users.all {it.age >= 30}
    users.all {it.age >= 10}

    //verifie si au moins un item verifie la cond
    users.any {it.age >= 20}
    users.any {it.age >= 40}

    //retourne le nb d'item verifiant la cond
    users.count{it.age >= 20}

    //retourne le premier item verifiant la cond
    users.find{ it.age >= 20 }

    */

}